package screens;


import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import asciiPanel.AsciiPanel;
import uvsq.projet.*;
public class PlayScreen implements Screen {
	private World world;
	private int centerX;
	private int centerY;
	private int screenWidth;
	private int screenHeight;
	Creature player;
	private List<String> messages;

	public PlayScreen(){
		messages = new ArrayList<String>();
		screenWidth = 80;// La longeur de notre zone de jeu
		screenHeight = 21;// la largeur de notre zone de jeu
		messages = new ArrayList<String>();
		createWorld();

		CreatureFactory creatureFactory = new CreatureFactory(world);
		createCreatures(creatureFactory);
	}
	public void displayOutPut(AsciiPanel terminal) {
		int left = getScrollX();
		int top = getScrollY();
		displayTiles(terminal, left, top);
		displayMessages(terminal, messages);
		terminal.write(player.glyph(), player.x - left, player.y - top, player.color());
		for(int i=0;i<79;i++)
		{
			terminal.write("-", i, 22,Color.ORANGE);
		}
		terminal.write("PLAY !" , 35, 23);
		String stats = String.format(" %3d/%3d Vie", player.hp(), player.maxHp());
		terminal.write(stats, 1, 23);

	}

	public Screen respondToUserInput(KeyEvent key) {
		switch (key.getKeyCode()) {
		case KeyEvent.VK_G: 
			Creature t=world.verifPlayerOnPotion(player);
			if(world.verifPlayerOnPotion(player)!=null)
			{
				if((player.hp()+t.hp()>player.maxHp()))
				{
					player.setHp(player.maxHp());
				}
				else 
				{
					player.setHp(this.player.hp()+t.hp());
				}
				world.remove(world.verifPlayerOnPotion(player));
			}
			break;

		case KeyEvent.VK_ENTER: return new WinScreen();
		case KeyEvent.VK_ESCAPE: return new LoseScreen();
		case KeyEvent.VK_LEFT:player.moveBy(-1, 0); break;
		case KeyEvent.VK_RIGHT: player.moveBy( 1, 0); break;
		case KeyEvent.VK_UP:player.moveBy( 0,-1); break;
		case KeyEvent.VK_DOWN:player.moveBy( 0, 1); break;
		default:
			break;
		}
		return this;
	}



	/**Creation du Player et des Monstres
	 * @param creatureFactory
	 */
	private void createCreatures(CreatureFactory creatureFactory){
		player = creatureFactory.newPlayer(messages);

		for (int i = 0; i < 30; i++){
			creatureFactory.newFungus();
		}

		for (int i = 0; i < 30; i++){
			creatureFactory.newPotionVie();
		}
	} 

	private void createWorld(){
		world = new WorldBuilder(90, 31)
				.makeCaves()
				.build();
	}

	public int getScrollX() {
		return Math.max(0, Math.min(player.x - screenWidth / 2, world.width() - screenWidth));
	}

	public int getScrollY() {
		return Math.max(0, Math.min(player.y - screenHeight / 2, world.height() - screenHeight));
	}

	private void displayTiles(AsciiPanel terminal, int left, int top) {
		for (int x = 0; x < screenWidth; x++){
			for (int y = 0; y < screenHeight; y++){
				int wx = x + left;
				int wy = y + top;
				Creature creature = world.creature(wx, wy);
				if (creature != null)
				{
					terminal.write(creature.glyph(), creature.x - left, creature.y - top, creature.color());

				}
				else
				{
					terminal.write(world.glyph(wx, wy), x, y, world.color(wx, wy));
				}
			}
		}
	}
	/*private void scrollBy(int mx, int my){
		centerX = Math.max(0, Math.min(centerX + mx, world.width() - 1));
		centerY = Math.max(0, Math.min(centerY + my, world.height() - 1));
	}*/

	private void displayMessages(AsciiPanel terminal, List<String> messages) {
		int top = screenHeight - messages.size();
		for (int i = 0; i < messages.size(); i++){
			terminal.writeCenter(messages.get(i), top + i);
		}
		messages.clear();
	}
}
