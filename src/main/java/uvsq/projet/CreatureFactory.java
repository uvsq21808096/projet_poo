package uvsq.projet;

import java.util.List;

import asciiPanel.AsciiPanel;
import screens.ChoiceChampion;


public class CreatureFactory {
	private World world;

	public CreatureFactory(World world){
		this.world = world;
	}

	/**
	 * @param messages
	 * @param code 1=Humain; 2= Zombie; 3=Alien;
	 * @return
	 */
	public Creature newPlayer(List<String> messages){
		Creature player;
		if(ChoiceChampion.code == 1)
		{
			player = new Creature(world, 'H', AsciiPanel.brightWhite, 100, 50,1);
		}
		else if(ChoiceChampion.code == 2)
		{
			player = new Creature(world, 'Z', AsciiPanel.brightWhite,250 , 15,1);
		}
		else 
		{
			player = new Creature(world, 'A', AsciiPanel.brightWhite, 50, 90,1);
		}
		world.addAtEmptyLocation(player);
		new PlayerAI(player, messages);
		return player;
	}
	public Creature newFungus(){
		Creature fungus = new Creature(world, 'f', AsciiPanel.white, 10, 10,2);
		world.addAtEmptyLocation(fungus);
		new FungusAi(fungus, this);
		return fungus;
	}
	public Creature newPotionVie(){
		Creature potionVie = new Creature(world, 'V', AsciiPanel.blue, 20, 0,3);
		world.addAtEmptyLocation(potionVie);
		new PotionVie(potionVie,this);
		return potionVie;
	}
}
