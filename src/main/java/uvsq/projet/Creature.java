package uvsq.projet;

import java.awt.Color;

public class Creature {
	private World world;
	
	public int x;
	public int y;
	public int type_creature; //1=player; 2=monstre; 3=potion
	private char glyph;
	public char glyph() { return glyph; }
	
	private Color color;
	public Color color() { return color; }

	private CreatureAi ai;
	public void setCreatureAi(CreatureAi ai) { this.ai = ai; }
	
	private int maxHp;
	public int maxHp() { return maxHp; }
	
	private int hp;
	public int hp() { return hp; }
	
	public void setHp(int hp) {
		this.hp = hp;
	}

	private int attackValue;
	public int attackValue() { return attackValue; }

	private int defenseValue;
	public int defenseValue() { return defenseValue; }
	
	public Creature(World world, char glyph, Color color, int hp, int attack, int type){
		this.world = world;
		this.glyph = glyph;
		this.color = color;
		this.hp = hp;
		this.maxHp = hp;
		this.attackValue = attack;
		this.type_creature=type;
	}
	
	public void moveBy(int mx, int my){
		Creature other = world.creature(x+mx, y+my);
		
		if (other == null)
			ai.onEnter(x+mx, y+my, world.tile(x+mx, y+my));
		else
			if(other.type_creature==2)
			{
				attack(other);
			}
			else
			{
				ai.onEnter(x+mx, y+my, world.tile(x+mx, y+my));
				doAction("find heal potion");
				doAction("can press [G] to take it");
			}
			
	}

	public void attack(Creature other){
		int amount = (int)(Math.random() * this.attackValue()) + 1; //On ajoute +1 car pour qu'il attaque pas avec 0 dmg "fonction random"
		
		doAction("attack the '%s' for %d damage", other.glyph, amount);
		int dmg= (int)(Math.random() * other.attackValue()) + 1;
		this.hp=hp-dmg;
		doAction("'%s' attack you with %d damage", other.glyph, dmg);
		other.modifyHp(amount);
	}

	public void modifyHp(int amount) { 
		hp = hp-amount;
		
		if (hp < 1) {
			doAction("die");
			world.remove(this);
		}
	}
	
	public void dig(int wx, int wy) {
		world.dig(wx, wy);
		doAction("can't dig");
	}
	
	public void update(){
		ai.onUpdate();
	}

	public boolean canEnter(int wx, int wy) {
		return world.tile(wx, wy).isGround() && world.creature(wx, wy) == null;
	}

	public void notify(String message, Object ... params){
		ai.onNotify(String.format(message, params));
	}
	
	public void doAction(String message, Object ... params){
		int r = 9;
		for (int ox = -r; ox < r+1; ox++){
			for (int oy = -r; oy < r+1; oy++){
				if (ox*ox + oy*oy > r*r)
					continue;
				
				Creature other = world.creature(x+ox, y+oy);
				
				if (other == null)
					continue;
				
				if (other == this)
					other.notify("You " + message + ".", params);
				else
					other.notify(String.format("The '%s' %s.", glyph, makeSecondPerson(message)), params);
			}
		}
	}
	
	private String makeSecondPerson(String text){
		String[] words = text.split(" ");
		words[0] = words[0] + "s";
		
		StringBuilder builder = new StringBuilder();
		for (String word : words){
			builder.append(" ");
			builder.append(word);
		}
		
		return builder.toString().trim();
	}
}
