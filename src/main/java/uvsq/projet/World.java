package uvsq.projet;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class World {
	private Tile[][] tiles;
	private int width;
	private int height;
	private List<Creature> creatures;
	private List<Creature> potions;

	public int width() { return width; }

	public int height() { return height; }

	public World(Tile[][] tiles){
		this.tiles = tiles;
		this.width = tiles.length;
		this.height = tiles[0].length;
		this.creatures = new ArrayList<Creature>();
		this.potions= new ArrayList<Creature>();
	}
	public Creature creature(int x, int y){
		for (Creature c : creatures){
			if (c.x == x && c.y == y)
				return c;
		}
		for (Creature c : potions){
			if (c.x == x && c.y == y)
				return c;
		}
		return null;
	}

	public Tile tile(int x, int y){
		if (x< 0 || x >= width || y< 0 || y >= height)
			return Tile.BOUNDS;
		else
			return tiles[x][y];
	}

	public char glyph(int x, int y){
		return tile(x, y).glyph();
	}

	public Color color(int x, int y){
		return tile(x, y).color();
	}

	public void dig(int x, int y) {
		if (tile(x,y).isWall())
		{
			//tiles[x][y] = Tile.FLOOR;
		}

	}

	public void addAtEmptyLocation(Creature creature){
		int x;
		int y;

		do {
			x = (int)(Math.random() * width);
			y = (int)(Math.random() * height);
		} 
		while (!tile(x,y).isGround() || creature(x,y) != null);

		creature.x = x;
		creature.y = y;
		if(creature.type_creature==3)
		{
			potions.add(creature);
		}
		else 
		{
			creatures.add(creature);
		}

	}

	public void update(){
		List<Creature> toUpdate = new ArrayList<Creature>(creatures);
		//List<Creature> potionToUpdate = new ArrayList<Creature>(potion);
		for (Creature creature : toUpdate){
			creature.update();
		}
		/*for (Creature creature : potionToUpdate){
			creature.update();
		}*/
	}

	public void remove(Creature other) {
		if(other.type_creature==2)
		{
			creatures.remove(other);
		}
		else 
			if(other.type_creature==3)
			{
				potions.remove(other);
				other.doAction("give you 20 hp");
			}

	}

	public Creature verifPlayerOnPotion(Creature player) { 
		
		for(Creature c:potions)
		{
			if((player.x==c.x && player.y==c.y))
			{
				return c;
			}
		}
		return null;
	}

}
